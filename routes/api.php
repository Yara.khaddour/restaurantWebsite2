<?php

use App\Http\Controllers\CartController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\MealController;
use App\Http\Controllers\ReservationController;
use App\Models\Reservation;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
 
Route::group(['middleware'=>['auth:sanctum']], function () {
    Route::get('/test/{id}', [OrderController::class, 'show']);
});

Route::post('/users/create', [UserController::class, 'store']);
Route::post('/login', [UserController::class, 'login']);
Route::get('/meals', [MealController::class, 'show']);
Route::get('/meal/{id}', [MealController::class, 'find']);
Route::get('/ordermeal/{id}', [OrderController::class, 'show']);
Route::post('/orderupdate/{id}', [OrderController::class, 'update']);
Route::post('/orderdestroy', [OrderController::class, 'destroy']);
Route::get('/res/{id}', [ReservationController::class, 'show']);
Route::post('/resupdate', [ReservationController::class, 'update']);
Route::post('/resdestroy', [ReservationController::class, 'destroy']);
Route::post('/restore', [ReservationController::class, 'store']);
Route::post('/place-order', [OrderController::class, 'store']);

Route::get('/getcart/{user_id}', [CartController::class, 'index']);
Route::post('/addcart', [CartController::class, 'addcart']);
Route::get('/deletecart/{cart_id}', [CartController::class, 'deletecart']);
//Route::get('/test/{id}', [OrderController::class, 'show']);
