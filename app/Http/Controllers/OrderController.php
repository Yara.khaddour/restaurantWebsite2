<?php

namespace App\Http\Controllers;

use App\Models\MealOrder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Order;
use App\Models\User;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::find($request->user_id);

        $carts = $user->carts()->get();

        $total = 0;

        foreach($carts as $cart) {
            $total += $cart->quantity * $cart->meal->price;
        }

        $order = Order::create([
            "user_id" => $user->id,
            "total_price" => $total
        ]);

        foreach($carts as $cart) {
            MealOrder::create([
                "order_id" => $order->id,
                "meal_id" => $cart->meal_id,
                "quantity" => $cart->quantity
            ]);
        }

        $user->carts()->delete();

        return response("تم وضع الطلب بنجاح", 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $order = Order::find($id);
        // return response()->json($order);
        $mealorder = Order::all();
        $response=[
            'mealorder'=>$mealorder,
        ];

        return response($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $product=Order::find($request->id);
        $product->update($request->all());
        return $product;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Order::destroy($id);
    }
}
