<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\User;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function index($user_id) {
        $user = User::find($user_id);
        $carts = $user->carts()->with('meal')->get();

        if(is_null($carts)) return response("لا يوجد منتجات في السلة", 404);
        
        $total_amount = 0;
        foreach($carts as $cart) {
            $cart_price = $cart->quantity * $cart->meal->price;
            $cart->setAttribute("price", $cart_price);
            $total_amount += $cart_price;
        }
        return response(["carts" => $carts, "total" => $total_amount], 200);
    }

    public function addcart(Request $request) {
        $meal_id = $request->meal_id;
        $quantity = $request->quantity;
        $user_id = $request->user_id;

        $previouscart = Cart::where("meal_id", $meal_id)->where("user_id", $user_id)->first();

        if(is_null($previouscart)) {
            Cart::create([
                "user_id" => $user_id,
                "meal_id" => $meal_id,
                "quantity" => $quantity
            ]);
        } else {
            $previouscart->quantity += $quantity;
            $previouscart->save();
        }

        return response("تمت الإضافة بنجاح", 200);
    }

    public function deletecart($cart_id) {
        Cart::find($cart_id)->delete();

        return response("تم الحذف بنجاح", 200);
    }
}
