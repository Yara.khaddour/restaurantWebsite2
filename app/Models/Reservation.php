<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;
    protected $fillable=[
        'id',
        'table_res_id',
        'user_id',
        'reservation_date'
    ];

    protected $dates=['reservation_date'];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function table_res(){
        return $this->belongsTo(Table_res::class);
    }
}
