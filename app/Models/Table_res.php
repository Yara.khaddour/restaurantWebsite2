<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Table_res extends Model
{
    use HasFactory;
    protected $fillable=[
        'id',
        'location',
        'count',
        'status'
    ];
    //protected $table = 'table_res';
    public function reservation(){
        return $this->hasMany(Reservation::class);
    }
}
