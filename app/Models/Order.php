<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $fillable=[
        'id',
        'user_id',
        'total_price'
    ];

    public function ordermeal(){
        return $this->hasMany(MealOrder::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }

    public function meals()
    {
        return $this->belongsToMany(Meal::class)->withPivot('quantity');
    }
}
