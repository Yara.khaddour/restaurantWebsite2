<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Cart extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function meal() {
        return $this->belongsTo(Meal::class, 'meal_id');
    }

    public function user() {
        return $this->BelongsTo(User::class, 'user_id');
    }
}
